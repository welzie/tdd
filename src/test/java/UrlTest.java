import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by LaunchCode
 */
public class UrlTest {

    @Test
    public void testConstructorSetsProtocol() {
        Url lcUrl = new Url("https://launchcode.org/learn");
        assertEquals("https", lcUrl.getProtocol());
    }

    @Test
    public void testConstructorSetsDomain() {
        Url lcUrl = new Url("https://launchcode.org/learn");
        assertEquals("launchcode.org", lcUrl.getDomain());
    }

    @Test
    public void testConstructorSetsPath() {
        Url lcUrl = new Url("https://launchcode.org/learn");
        assertEquals("learn", lcUrl.getPath());
    }

    @Test
    public void testToStringIdealFormat() {
        String urlStr = "https://launchcode.org/learn";
        Url lcUrl = new Url(urlStr);
        assertEquals(urlStr, lcUrl.toString());
    }

    @Test
    public void testConstructorConvertsCaseToLower() {
        String urlStr = "HTTPS://lAuNcHcOdE.org/LEARN";
        Url lcUrl = new Url(urlStr);
        assertEquals("https://launchcode.org/learn", lcUrl.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidProtocolFromConstructorThrowsException() {
        String urlStr = "htt://launchcode.org/learn";
        new Url(urlStr);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyDomainThrowsException() {
        String urlStr = "htt:///learn";
        new Url(urlStr);
    }

    @Test
    public void testEmptyPathIsValid() {
        String urlStr = "https://launchcode.org";
        Url lcUrl = new Url(urlStr);
        assertEquals("", lcUrl.getPath());
    }

    @Test
    public void testDomainAllowedCharacters() {
        String urlStr = "https://abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_-1234567890.org";
        Url url = new Url(urlStr);
        assertEquals("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz_-1234567890.org", url.getDomain());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDomainIllegalCharacters() {
        String urlStr = "http://&.org";
        new Url(urlStr);
    }
}

import java.util.Arrays;
import java.util.List;

public class Url {
    private final String protocol;
    private final String domain;
    private final String path;

    private static final List<String> VALID_PROTOCOLS = Arrays.asList("ftp", "http", "https", "file");

    public Url(String url) {

        String[] parts = url.split("/");
        this.protocol = parts[0].split(":")[0].toLowerCase();
        if (!VALID_PROTOCOLS.contains(this.protocol)) {
            throw new IllegalArgumentException("Invalid protocol");
        }

        this.domain = parts[2].toLowerCase();
        if (!this.domain.matches("[\\w\\d\\._-]+")) {
            throw new IllegalArgumentException("Invalid domain");
        }

        this.path = parts.length > 3 ? parts[3].toLowerCase() : "";
    }

    public String getProtocol() {
        return protocol;
    }

    public String getDomain() {
        return domain;
    }

    public String getPath() {
        return path;
    }

    public String toString() {
        return getProtocol() + "://" + getDomain() + "/" + getPath();
    }
}
